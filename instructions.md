---
title: Trello URL deliverable
author: #module_leder
documentclass: hitec
fontfamily: opensans
fontsize: 12
...
# Objectives

For this exercise, we will create a Kanban board using the service at
Trello.com.  You may want to review the Project planning tutorial
notes regarding Kanban first.

# Tasks

## Part 1: Create a Kanban board using Trello.

#. Register for a Trello account.  Visit <www.trello.com>, and
 enter your email address in the text box next to the green "Sign Up - It's Free!" button:\
    ![Trello Sign-up](trello-signup.png)\
    
#. Select a member of your group to be the Trello board owner.  Your group should have _one_ board to manage the project plan.

#. The Trello board owner should create a new Kanban board.    Trello will ask you to specify a name
 for your Kanban board.  Name it after *your* group (do **NOT** name
 it _A_group_999_!):\
    ![Kanban board name](trello-board-title.png)\
    

#. Invite all team members to join the board.  You should use their
 #email(herts.ac.uk) email addresses so they get credit for their contributions.

    ![Invite ML](trello-invite-ml.png)\


#. Invite your module leader to join the board (use #email(j.noll@herts.ac.uk)).



#. Create four lists (columns) called "Backlog," "Doing," "Review," and "Done."\
    ![Kanban columns](trello-four-lists.png)\
    

#. Create a work breakdown structure for your literature review
    project.  Recall that you need to:

    * Define a research question.
    * Specify inclusion and exclusion criteria.
    * Create a search string.
    * Execute the search to develop a candidate paper list.
    * Apply your inclusion/exclusion criteria to refine the list.
    * Extract data from the resulting list.
    * Synthesize the data into an answer to your research question.
    * Write your final report.

    Review the Literature Review lecture and notes for more information.


#. Create a card in the Backlog column for each _leaf_ in your work breakdown structure.
    ![Kanban columns](trello-add-card.png)\


#.  Add a “member” to each card. This person is the responsible party
    for the task represented by the card (although is not necessarily
    the person who will do the task; the responsible party may
    delegate sub-tasks to other group members).
    ![Add member](trello-add-member.png)\

 
#. Copy your Trello board URL.\

    ![Find URL](trello-find-url.png)\
    
#. Paste your Trello board URL into the URL submission box in the
 "Trello board URL" assignment on Canvas.\

    ![Submit URL](canvas-submit-url.png)\
    
#. Verify your URL!

    Someone other than the submitter should copy the URL from Canvas
to a web browser to be sure the submitter submitted the correct URL.

    You should submit _one_ and only _one_ Trello board URL: it should
be the URL of the board you are using to manage your project plan for
the entire semester.  You cannot change your Trello board URL after
you submit.
